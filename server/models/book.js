import mongoose from 'mongoose'
const Schema = mongoose.Schema

const bookSchema = new Schema({
  title: String
})

const bookModel = mongoose.model('book', bookSchema)

export default bookModel
