import bodyParser from 'body-parser'
import express from 'express'
import https from 'https'
import http from 'http'
import fs from 'fs'
import morgan from 'morgan'
import router from './router'
import mongoose from 'mongoose'
import cors from 'cors'

const app = express()

const options = {
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.crt')
}

// DB Setup
mongoose.connect('mongodb://localhost:27017/auth')



// App Setup
app.use(morgan('combined'))
app.use(cors())
app.use(bodyParser.json({ type: '*/*' }))

app.use('/api', router)

// Server Setup
const port = process.env.PORT || 3030
// const server = https.createServer(options, app) // use for https
const server = http.createServer(app)

server.listen(port, () => {
  console.log(`server listening on ${port}`)
})
