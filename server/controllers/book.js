import Book from '../models/book'

export default {
  addBook: function(req, res, next) {
    let body = req.body;
    let title = body.title;

    if(!title) {
      return res.status(422).send({ error: 'No book information was provided' })
    }

    const book = new Book({
      title
    })

    book.save(function(err) {
      if(err) { return next(err) }

      res.json({ status: "Book was saved!" })
    })

  }
}

// Saving a book
// 1) User fills out information
// 2) User submits information to '/api/books/addBook'
// 3) Server gets book information from req.body
// 4) Server saves book to Books.collection
// 5) Server adds Book._id to user.active_listings array
