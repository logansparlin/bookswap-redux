import User from '../models/user'
import jwt from 'jwt-simple'
import config from '../config'

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret)
}

export default {
  signup: function(req, res, next) {
    const email = req.body.email;
    const password = req.body.password;
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;

    let validEmail = function(email) {
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return re.test(email)
    }

    // If no email or password, return an error status
    if(!email || !password) {
      return res.status(422).send({ error: 'You must provide an email and password' })
    }

    if(!validEmail(email)) {
      return res.status(422).send({error: 'Not a valid email address'})
    }

    // See if user w/ email exists
    User.findOne({ email: email }, function(err, existingUser) {
      if(err) { return next(err) }

      // If a user w/ email exists, return Error
      if(existingUser) {
        return res.status(422).send({ error: 'Email is already in use' });
      }

      // If user w/ email does NOT exist, create & save user record
      const user = new User({
        email,
        password,
        firstName,
        lastName
      })

      user.save(function(err) {
        if(err) { return next(err) }

        //  Respond to request, user was created
        res.json({ token: tokenForUser(user), id: user._id })
      })
    })
  },

  login: function(req, res, next) {
    // User has already had their email & password authorized
    // We need to give them a token
    res.json({
      token: tokenForUser(req.user),
      id: req.user._id
    })
  },

  fetchUserData: function(req, res, next) {
    User.findOne({_id: req.headers._id}, {password: 0}, function(err, user) {
      if(err) { return next(err) }
      res.json({ user: user})
    })
  }
}
