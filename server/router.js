import Auth from './controllers/auth'
import Book from './controllers/book'
import passportService from './services/passport'
import passport from 'passport'
import express from 'express'
var router = express.Router()

const requireAuth = passport.authenticate('jwt', { session: false })
const requireLogin = passport.authenticate('local', { session: false })

router.get('/', requireAuth, function(req, res) {
  res.send({message: 'Secret code is ABC123'})
})

router.post('/login', requireLogin, Auth.login)
router.post('/signup', Auth.signup)
router.get('/userById', requireAuth, Auth.fetchUserData)
router.post('/books/addBook', requireAuth, Book.addBook)

export default router
