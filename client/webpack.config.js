var jeet = require('jeet');
var rupture = require('rupture');
var autoprefixer = require('autoprefixer-stylus');

module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    path: __dirname,
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        exclude: /node_modules/,
        test: /\.js$/,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015', 'stage-1', 'react-hmre']
        }
      },
      {
        test: /\.styl$/,
        loader: 'style-loader!css-loader!stylus-loader?paths[]=public'
      }
    ]
  },
  stylus:{
    use: [jeet(),rupture(),autoprefixer()]
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.styl']
  },
  devServer: {
    historyApiFallback: true,
    hot: true,
    contentBase: './',
    port: 4040
  }
};
