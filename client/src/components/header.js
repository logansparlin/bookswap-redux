import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import './header.styl'

class Header extends Component {

  renderLinks() {
    let { authenticated, user } = this.props;
    console.log(this.props)
    if(authenticated) {
      return [
        <li key={1} className="nav-item">
          <Link className="nav-link" to="/logout">Logout</Link>
        </li>,
        <li key={2} className="nav-item">
          <Link className="nav-link" to="/sell">Sell</Link>
        </li>
      ]
    } else {
      return [
        <li className="nav-item" key={1}>
          <Link className="nav-link" to="/login">Login</Link>
        </li>,
        <li className="nav-item" key={2}>
          <Link className="nav-link" to="/signup">Sign Up</Link>
        </li>
      ]
    }
  }

  render() {
    return (
      <nav className="navbar">
        <Link to="/" className="navbar-brand">Bookswap</Link>
        <ul className="nav navbar-nav">
          {this.renderLinks()}
        </ul>
      </nav>
    )
  }
}

function mapStateToProps(state) {
  return {
      authenticated: state.auth.authenticated,
      user: state.user
  }
}

export default connect(mapStateToProps)(Header)
