'use strict';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as actions from '../actions'
import './main.styl'

class Welcome extends Component {

  render() {
    let { books } = this.props
    return (
      <div>
        <h1>Welcome to the site!</h1>
      </div>
    );
  }

}

function mapStateToProps(state) {
  return {
    books: state.books
  }
}

export default connect(mapStateToProps, actions)(Welcome);
