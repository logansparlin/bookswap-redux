'use strict';
import React, { Component } from 'react';
import { reduxForm } from 'redux-form'
import * as actions from '../../actions'

class Signup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      passwordHidden: true
    }
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
    this.togglePasswordVisibility = this.togglePasswordVisibility.bind(this)
  }

  handleFormSubmit(formProps) {
    this.props.signupUser(formProps)
  }

  togglePasswordVisibility() {
    this.setState({ passwordHidden: !this.state.passwordHidden })
  }

  renderAlert() {
    if(this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      )
    }
  }

  render() {
    const { handleSubmit, fields: {firstName, lastName, email, password}} = this.props
    const {passwordHidden} = this.state;

    return (
      <form onSubmit={handleSubmit(this.handleFormSubmit)}>
        <h1>Signup</h1>
        <fieldset className="form-group">
          <label>First Name:</label>
          <input {...firstName} type="text" className="form-control" />
          {firstName.touched && firstName.error && <div className="error">{firstName.error}</div>}
        </fieldset>
        <fieldset className="form-group">
          <label>Last Name:</label>
          <input {...lastName} type="text" className="form-control" />
          {lastName.touched && lastName.error && <div className="error">{lastName.error}</div>}
        </fieldset>
        <fieldset className="form-group">
          <label>Email:</label>
          <input {...email} type="email" className="form-control" />
          {email.touched && email.error && <div className="error">{email.error}</div>}
        </fieldset>
        <fieldset className="form-group">
          <label>Password:</label>
          <input {...password} type={(!passwordHidden) ? 'text' : 'password'} className="form-control" />
          <span onClick={this.togglePasswordVisibility}>Show Password</span>
          {password.touched && password.error && <div className="error">{password.error}</div>}
        </fieldset>
        {this.renderAlert()}
        <button action="submit" className="btn btn-primary">Sign up</button>
      </form>
    );
  }
}

function validate(formProps) {
  const errors = {}

  Object.keys(formProps).map(prop => {
    if(!formProps[prop]) {
      let prettyProp = prop.charAt(0).toUpperCase() + prop.slice(1).replace('Name', ' name')
      errors[prop] = `${prettyProp} is required`
    }
  })

  if(formProps.password && formProps.password.length <= 5) {
    errors.password = "Password must contain at least 6 characters"
  }

  return errors;
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error
  }
}

export default reduxForm({
  form: 'signup',
  fields: ['firstName', 'lastName', 'email', 'password'],
  validate
}, mapStateToProps, actions)(Signup);
