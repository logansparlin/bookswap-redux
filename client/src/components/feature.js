'use strict';
import React, {Component } from 'react';
import { connect } from 'react-redux'
import * as actions from '../actions'

class Feature extends Component {

  componentWillMount() {
    this.props.fetchMessage()
    this.props.fetchUserData()
  }

  render() {
    let { user, message } = this.props
    return (
      <div>
        <h2>Hello, {user.firstName} {user.lastName}</h2>
        <h4>{message}</h4>
      </div>
    );
  }

}

function mapStateToProps(state) {
  return {
    message: state.auth.message,
    user: state.user
  }
}

export default connect(mapStateToProps, actions)(Feature);
