'use strict';
import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import './sell.styl'

class Sell extends Component {

  constructor(props) {
    super(props)
    this.renderSelectedBook = this.renderSelectedBook.bind(this)
    this.state = {
      selected_book: null
    }
  }

  componentDidMount() {
    this.props.fetchBooks()
  }

  renderImage(links) {
    if(links) {
      let url = links.thumbnail.replace('&zoom=1', '&zoom=2')
      return <img src={url} />
    }
  }

  selectBook(book) {
    console.log(book)
    this.setState({ selected_book: book})
  }

  renderSelectedBook() {
    let { selected_book } = this.state;
    if(selected_book) {
      console.log('selected book exists')
      return <div>
        <h1>{selected_book.volumeInfo.title}</h1>
      </div>
    }
  }

  render() {
    let { books } = this.props
    return (
      <div className="container book-search">
        <input type="search" onChange={(e) => {this.props.fetchBooks(e.target.value)}} type="text" placeholder="Search by Title or ISBN"/>
        <div>{books.query}</div>
        <div className="search-books-results">
          {books.books.map(book => {
            return <div className="book" key={book.id} onClick={this.selectBook.bind(this, book)}>
              {this.renderImage(book.volumeInfo.imageLinks)}
              <h5>{book.volumeInfo.title}</h5>
              <div>{(book.volumeInfo.authors) ? book.volumeInfo.authors.join(', ') : null}</div>
            </div>
          })}
        </div>
        {this.renderSelectedBook()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    books: state.books
  }
}

export default connect(mapStateToProps, actions)(Sell);
