import axios from 'axios'
import { browserHistory } from 'react-router'

import {
  AUTH_USER,
  UNAUTH_USER,
  AUTH_ERROR,
  FETCH_MESSAGE,
  FETCH_USER,
  FETCH_BOOKS,
  SET_SEARCH_TEXT
} from './types'

const ROOT_URL = 'http://localhost:3030/api'

export function signupUser({ firstName, lastName, email, password }) {
  return function(dispatch) {
    axios.post(`${ROOT_URL}/signup`, {firstName, lastName, email, password})
      .then(res => {
        dispatch({ type: AUTH_USER })
        localStorage.setItem('token', res.data.token)
        localStorage.setItem('_id', res.data.id)
        browserHistory.push('/feature')
      })
      .catch(err => {
        dispatch(authError('Email already in use'))
      })
  }
}

export function loginUser({ email, password }) {
  return function(dispatch) {
    // Submit email & pass to server
    axios.post(`${ROOT_URL}/login`, { email, password })
      .then(res => {
        // If request is good...
        // - update state to indicate user is authenticated
        dispatch({ type: AUTH_USER })
        // - Save JWT token
        localStorage.setItem('token', res.data.token)
        localStorage.setItem('_id', res.data.id)
        // - redirecto to the route '/feature'
        browserHistory.push('/feature')
      })
      .catch(err  => {
        // If request is bad...
        // - Show an error to the user
        dispatch(authError('Bad Login Info'))
      })
  }
}

export function authError(error) {
  return {
    type: AUTH_ERROR,
    payload: error
  }
}

export function logoutUser() {
  // unset token
  localStorage.removeItem('token')
  localStorage.removeItem('_id')

  return { type: UNAUTH_USER }
}

export function fetchMessage() {
  return function(dispatch) {
    axios.get(ROOT_URL, {
      headers: {
        authorization: localStorage.getItem('token')
      }
    })
      .then(res => {
        dispatch({
          type: FETCH_MESSAGE,
          payload: res.data.message
        })
      })
  }
}

export function fetchUserData() {
  return function(dispatch) {
    axios.get(`${ROOT_URL}/userById`, {
      headers: {
        authorization: localStorage.getItem('token'),
        _id: localStorage.getItem('_id')
      }
    })
    .then(res => {
      dispatch({
        type: FETCH_USER,
        payload: res.data.user
      })
    })
  }
}

export function fetchBooks(query) {
  return function(dispatch) {
    if(query) {
      let formattedQuery = query.toLowerCase().split(' ').join('+')
      axios.get(`https://www.googleapis.com/books/v1/volumes?q=${formattedQuery}&orderBy=relevance&maxResults=8&key=AIzaSyApUSiddfk5SFAzatjDy6I8BUNBUm7Ue68`)
      .then(res => {
        console.log(res.data.items)
        dispatch({
          type: FETCH_BOOKS,
          payload: res.data.items
        })
      })
    } else {
      dispatch({
        type: FETCH_BOOKS,
        payload: []
      })
    }
  }
}

export function setSearchText(str) {
  return {
    type: SET_SEARCH_TEXT,
    payload: str
  }
}
