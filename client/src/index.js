import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import reduxThunk from 'redux-thunk'
import { AUTH_USER } from './actions/types'

import App from './components/app';
import Login from './components/auth/login'
import Signup from './components/auth/signup'
import Logout from './components/auth/logout'
import Feature from './components/feature'
import Welcome from './components/welcome'
import Sell from './components/sell/sell'
import RequireAuth from './components/auth/require_auth'
import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers)

const token = localStorage.getItem('token')
// If we have a token, user is signed in
if(token) {
  // update app state
  store.dispatch({ type: AUTH_USER })
}

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={Welcome} />
        <Route path="/login" component={Login} />
        <Route path="/signup" component={Signup} />
        <Route path="/logout" component={Logout} />
        <Route path="/feature" component={RequireAuth(Feature)} />
        <Route path="/sell" component={RequireAuth(Sell)} />
      </Route>
    </Router>
  </Provider>
  , document.querySelector('.main'));
