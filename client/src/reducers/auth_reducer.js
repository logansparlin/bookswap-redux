import {
  AUTH_USER,
  UNAUTH_USER,
  AUTH_ERROR,
  FETCH_MESSAGE
} from '../actions/types'

const initialState = {
  authenticated: false,
  message: null,
  error: null,
  user: {}
}

export default function(state = initialState, action) {
  switch(action.type) {
    case AUTH_USER:
      return {
        ...state,
        authenticated: true,
        error: null
      }
    case UNAUTH_USER:
      return {
        ...state,
        authenticated: false,
        error: null
      }
    case AUTH_ERROR:
      return {
        ...state,
        error: action.payload
      }
    case FETCH_MESSAGE:
      return {
        ...state,
        message: action.payload
      }
  }

  return state
}
