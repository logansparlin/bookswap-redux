import {
  FETCH_BOOKS,
  SET_SEARCH_TEXT
} from '../actions/types'

const initialState = {
  query: '',
  books: []
}

export default function(state = initialState, action) {
  switch(action.type) {
    case FETCH_BOOKS:
      return {
        ...state,
        books: action.payload
      }
    case SET_SEARCH_TEXT:
      return {
        ...state,
        query: action.payload
      }
  }

  return state
}
